//1
var myArray=["hana","wael","hello"];
//4
function emptyArray() {
   var array=[]; 
    return array;
} 
//5
function numbersArray() {
   var array=[1,2,3,4,5];
   return array; 
}
//6
function booleansArray() {
   var array=[true,true,true] ;
   return array;
}
//7
function stringsArray() {
   var array=["wael", "hajji"] 
   return array;
}
//8
function arrayLength(array) {
   return array.length; 
} 
//more practice
//1 return first element in an array
function firstElement(array) {
    return array[0];
}
//2 return the last element in an array
function lastElement(array) {
   return array[array.length-1]; 
}
//3 get the nth element in an array
function getNthElement(array, number) {
   return array[number] ;
}
//4  returns a new array with the element added to the end.
function push(array, element) {
   array.push(element);
   return array; 
}
//5 returns a new array with the last element removed from it
function pop(array) {
   //array.pop();
   array.splice(array.length-1, 1);
   return array; 
}
//6 returns a new array with the element added to the front.
function unshift(array, element) {
   //array.unshift(element);
   array.splice(0, 1,element);
    return array; 
}
//7 returns a new array with the first element removed.
function shift(array) {
  array.splice(0, 1);
   //array.shift();
return array; 
} 
//8 returns a new array with the last element of the original array replaced with the new element.
function reassignLast(array, element) {
   array.pop();
   array.push(element);
   return array; 
}
//9 returns a new array with the new value in the place of the element at the specified index.
function reassignNthElement(array, index, element) {
   array[index]=element;
   return array; 
}
//advanced
//1 returns the index of the array where the element is located.
function indexOf(array, element) {
   var i=0;
    while(i<array.length){ 
        if (array[i]===element){
            return i;}
        i++;
}
}
//2 returns an array with that element added in 
//the place of that index but nothing in the array is removed. NOTE: Please use this exercise to practice .splice()
function addElement(array, index, element) {
   array.splice(index,0,element);
   return array; 
}
//3 returns an array with the element in that index removed. NOTE: Please use this exercise to practice .splice()
function removeElement(array, index) {
   array.splice(index,1);
   return array; 
}
//4 returns a new array that combines all elements of the two parameters
function concatArrays(array1, array2) {
  //array1 = array1.concat(array2);
   var i=0;
    while(i<array2.length){
        array1.push(array2[i]);
        i++;
    } 
    return array1;
}
//5 returns a new array that combines all elements of the two parameters
function concatThreeArrays(array1, array2,array3) {
  // array1 = array1.concat(array2);
 // array1 = array1.concat(array3);
   var i=0;
    while(i<array2.length){
                array1.push(array2[i]);
        i++;
    } 
    i=0;
    while(i<array3.length){
    array1.push(array3[i]);
    i++;
    }
    return array1;
} 
//6 takes two parameters, an array of strings and one string,
// and returns a new string that combines all of the strings from the array separated by the string parameter. NOTE: Use .join()
function joinStrings(array, string) {
   return array.join(string); 
}
//7 takes two parameters, a string to be split and a splitter (which is also a string),
// and returns an array that is the result of splitting the string on the splitter. NOTE: Use .split()
function applySplit(string, splitter) {
   return string.split(splitter); 
}
//8 that takes two parameters, an array and index, 
//and returns an array with all the elements after the index but does not include the element at the given index.
function getElementsAfter(array, index) {
  //return array.splice(0, index+1);
  var i =index+1;
  var array2=[];
  while(i<array.length){
     array2.push(array[i]);
     i++;
  }
return array2;
}
//9  that takes two parameters, an array and index, 
//and returns an array with all the elements before the index but does not include the element at the given index.

function getElementsBefore(array, index) {
  //return array.splice(index, array.length-index);
  var i =0;
  var array2=[];
  while(i<index){
     array2.push(array[i]);
     i++;
  }
return array2;
} 
//10 takes two parameters, an array and index, 
//and returns an array with all the elements before the index but does not include the element at the given index.
function getAllElementsButFirst(array) {
   array.shift();
   return array; 
}
//11 that takes an array as a parameter and returns an array with all the elements but the last element.
function getAllElementsButLast(array) {
   array.pop();
   return array; 
}









