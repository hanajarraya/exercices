//1 We have two ways of writing a function. The function declaration is what we've used so far, and the function expression is new to us. Rewrite the following function declarations using a function expression:

// 1. 
 function cube(x) { 
       return x * x * x; 
 } 
 // 2. 
 function fullname(first, last) { 
       return first + ' ' + last; 
 } 
 // 3. 
 function power(base, exp) { 
       if (exp === 0) { 
       return 1; 
       } 
       return base * power(base, exp - 1); 
 } 
 // 4. 
 function sumCubes(numbers) { 
       var total = 0; 
       for (var i = 0; i < numbers.length; i++) { 
             total = total + cube(numbers[i]); 
       } 
       return total; 
 }
function square(x) { 
    return x * x; 
} 
//answer of question 1
var square = function(x) { 
    return x * x; 
}; 

var cube = function(x){
return x*x*x;
}

var fullname=function(first,last){
return first +' '+last;
}

var power = function(base,exp){
if (exp===0){
return 1;
}
return base * power(base,exp -1);
}

var sumCubes=function(numbers){
var total =0;
for (var i=0 ; i<numbers.length;i++){
total=total +cube(numbers[i]);
}
return total;
}


//2 Write each as seen below in your main.js file.


function each(array, func) { 
    for (var i = 0; i < array.length; i++) { 
          func(array[i]); 
    } 
}
//3 Finish the implementation of sumSquares below (using each):
function sumSquares(numbers) { 
    var array=[];       
    var total = 0; 
    each(numbers,function(number){
    
    array.push(number*number)
    }
    )
    each(array,function(number){
    total+=number;
    })
    return total;
}
 //4 Rewrite sumCubes using each:

 function sumCubes(numbers) { 
       var total = 0; 
       for (var i = 0; i < numbers.length; i++) { 
             total = total + cube(numbers[i]); 
       } 
       return total; 
 }
 function sumCubes(numbers) { 
    var array=[];       
    var total = 0; 
    each(numbers,function(number){
    
    array.push(number*number*number)
    }
    )
    each(array,function(number){
    total+=number;
    })
    return total;
    
    }
 //5 Write a function called product that calculates the product of an array of numbers using a for loop; then, refactor it to use each
 //with for loop
 function product(array){
    var result=1;
    for (var i=0;i<array.length;i++){
    result*=array[i];
    }
    return result;
    }
///with each 

function product(array){
    var result=1;
    each(array,function(number){
    result*=number
    })
    return result;
    }

//6 Write a function called cubeAll that 
//cubes each number in an array, and returns an array of all the numbers cubed using a for loop; then, refactor it to use each.
//with each
function cubAll(array){
    var result=[];
    each(array,function(number){
    result.push(number*number*number)
    })
    return result;
    }



//with for lop
function cubAll(array){
    for (var i=0;i<array.length;i++){
    array[i]=array[i]*array[i]*array[i];
    }
    return array;
    }
//7 Write a function called odds that accepts an array as 
//a parameter and returns an array of just the odd numbers. If you wrote it using each, great! If you used anything else, refactor it to use each.

function oddss(array){
    var result=[];
    each(array,function(number){
    if (number%2!=0){
    
    result.push(number)
    }
    })
    return result;
    }
//MORE PRACTICE
//1 Write a function sumByAllElementsMultipliedByFour that takes an array as an argument and returns the sum of all elements multiplied by four.
function sumByAllElementsMultipliedByFour(numbers) { 
    var array=[];       
    var total = 0; 
    each(numbers,function(number){
    
    array.push(number*4)
    }
    )
    each(array,function(number){
    total+=number;
    })
    return total;
    
    }
//2 Observe that sumByAllElementsMultipliedByFour is a terrible name for a function – 
//you should also notice that sumByAllElementsMultipliedByFour looks a lot like sumCubes and sumSquares. Write a function sumBy that accepts two arguments: 
//an array of numbers and a function. The function will be invoked upon each element in the array, and its result will be used to compute the sum.

//3 How can you use each to compute the sum of an array of numbers (just the plain sum)?

//4 Write a function productBy that works like sumBy, but for products.

//5 As an extended exercise, run back through your data modeling code from the past few days and look for opportunities to refactor your use of for loops using each.

//advanced

//1 1.Write a function doubleAll that takes an array of numbers as a parameter and returns an array of all of those numbers doubled:


//2  Write a function halveAll that takes an array of numbers as a parameter and returns an array of all of those numbers halved (divided by two).


//3 Write a function uppercaseAll that takes an array of strings as a parameter, and returns an array of all of those strings,
// but transformed to upper case. You can use toUpperCase to convert a string to upper case.

//4 You should at this point notice a similarity between all of the above functions, as well as the cubeAll function from the basic requirements.
// These functions all define what we call mappings; that is, they map from one value to another.


//5 Write a function map that takes two parameters: an array and a function that, when applied to a single element, produces a new element. 
//map should return an array of all elements in the input array transformed using the input function. Your function should work like this:


//6 Complete the invocations of map below to produce the desired output (you'll need to replace ??? with a function):


//7 Write a function called evens that takes an array of numbers as a parameter, and returns an array of only the even numbers in the parameter.

//8.Write a function called multiplesOfThree that takes an array of numbers as a parameter, 
//and returns an array of only the numbers that are multiples of three.


//9 Write a function called positives that takes an array of numbers as a parameter, and returns an array of only the numbers that are positive.

//10.Write a function called evenLength that takes an array of strings and returns an array of only the strings with an even length

//11.At this point, you should notice a pattern; write a function called filter that takes two parameters: an array and a function that, 
//when invoked with an argument, will return true or false. filter should return a new array of only the elements for which the function returns true:

//12 Use filter to write/rewrite:
//odds 
 //positives 
 //negatives 
 //evenLength 
 //largerThanSix 
