//1 Write a function called select that, given 
//an object and an array of strings representing keys, returns a new object consisting of just the keys (and values) specified in the array of strings.
function select (object,keys){
var result={}
for(var key in object){
    for(var i=0;i<keys.length;i++){
        if(key===keys[i]){
            result[key]=object[key];
        }
    }
}
return result;
}
//2 Write a function called pluck that, given an array of objects as its first parameter 
//and a string representing a key as its second parameter, returns an array of all the values found in each object in the array by using string as a key.
 var people = [ 
       {name: {first: 'Alyssa', middle: 'P.', last: 'Hacker'}, age: 26}, 
       {name: {first: 'Louis', last: 'Reasoner'}, age: 21}, 
       {name: {first: 'Ben', last: 'Bitdiddle'}, age: 34}, 
       {name: {first: 'Eva', middle: 'Lu', last: 'Ator'}, age: 40}, 
       {name: {first: 'Lem', middle: 'E.', last: 'Tweakit'}, age: 45} 
       ]; 
function pluck(array,key){
var result =[];
for(var i=0;i<array.length;i++){
    result.push(array[i][key]);
}
return result;
}
//3 Write a function called extend that accepts two parameters, a destination object and a source object. extend should copy all of the keys and values from the source object (the second parameter)
// to the destination object (the first parameter), and return the destination object afterwards. You'll want to use our new version of each for this.
function extend(destination,source){
each(source,function(element,key){
            
                destination[key]=element;

});
return destination;
}
//4 .Complete the below function called zip that takes two arrays as parameters and combines them to form a single object. The first 
//array parameter represents the keys of the object, and the second parameter represents the values (you can assume the two arrays are the same length).
function zip(keys,values){
var result={};
for(var i=0;i<keys.length;i++){
result[keys[i]]=values[i];
}
return result;
}
//5 writ a function called sortAscending that takes an object, and returns array of sorted object values
function sortAscending(object){
var result=[]
var array=Object.values(object);
array=array.sort(function(a, b) {
  return a - b;
});

for (var i=0;i<array.length;i++){
    for(var key in object){
    console.log("key : ",key)
    if (array[i]===object[key]){
    var obj={}
    obj[key]=array[i]
    result.push(obj)
    }}
                }
return result;
}
//6 write a function called numbersOnly that takes an array of objects, and returns 
//the same array including the same objects but without the keys that are not numbers. you have to use another function to check for the type of the keys.
//draft
function numbersOnly(obj){

for(var i=0;i<obj.length;i++){
  
   each(obj[i],function (element,key){
        console.log("type ",typeof Number(key))
    if (typeof Number(key)!="number"){
        
        delete element;}
  });
}
return obj;
}