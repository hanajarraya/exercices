//1 Write a function, funcCaller, that takes 
//a func ( a function that returns the squared value of the parameter) and arg (a number). funcCaller should return the func called with arg(as a parameter).
function funcCaller(squared,number){
    return squared(number);
}

function squared(number){
     return number*number;
}
//2 Write a function, firstVal, 
//that takes an array, arr, and func ( a function that returns cubed value of the parameter), and calls func with the first index of the arr.
function firstVal(array,cubed){
    return cubed(array[0]);
}
undefined
function cubed(number){
    return number*number*number;
}
//more practice
//1 Implement a function,sumAll, that takes two numbers , a and b, and callback(a function), the callback is 
//a two-parameter function, that takes two numbers and returns the sum of them.sumAll should use the callback function to return the summation of a and b.
function sumAll(a,b,callBack){
    return callBack(a,b);
}
function callBack(a,b){
return a+b;
}
//2 Create a function called addS that takes two parameters: 1.word (as a string). 2. a callback function that accepts a string and adds an 's' to it. addS 
//function should return the callback function called with word (as a parameter). addS should use the callback function to return the summation of a and b.
function addS(word,callBack){
    return callBack(word,"s");
}
//3 Write a function, ftoc, that takes a number (temperature) and callback( a function), the callback is a one parameter function that 
//converts temperatures from Fahrenheit to Celsius.Ftoc function will use the callback function to return the Fahrenheit temperature converted to Celsius
function ftoc(number,convertToCelsius){
    return convertToCelsius(number);
}
function convertToCelsius(number){
    return (number-32)/1.8;
}
//advanced
//1 Write a function, multiplyByThree(), that accepts two parameters,myArray and callback, myArray is an array of numbers and callback is a one-parameter function, 
//that takes a value and multiplies it by three and returns it,multiplyByThree() function should return an array of all of those numbers multiplied by three.
function multiply(number){
    return number*3;
}
undefined
function multiplyByThree(array,multiply){
    for(var i=0;i<array.length;i++){
        array[i]=multiply(array[i]);
    }
return array;
}
//2 Write a function,leapYears, that takes two parameters, arrayOfYears, and callback,arrayOfYears is an array of years and callback is 
//a one-parameter function, that takes a year, determines whether or not a given year is a leap year. Leap years are determined 
//by the following rules:Leap years are years divisible by four (like 1984 and 2004). However, years divisible by 100 are 
//not leap years (such as 1800 and 1900) unless they are divisible by 400 (like 1600 and 2000,which were in fact leap years). 
//(Yes, it's all pretty confusing, but not as 
//confusing as having July in the middle of the winter, which is what would eventually happen.). leapYears() function should return an array of all leap years
function leapYears(arrayOfYears,isLeap){
    var leaps=[]
    for (var i=0;i<arrayOfYears.length;i++){
        if (isLeap(arrayOfYears[i])){
            leaps[i]="leap";
        } else {leaps[i]="not leap";}
    }
}
return leaps;
function isLeap(year){
    if (year%4===0 && year %100!=0){
        return true;
    }
    if (year %100===0 && year%400===0){
        return true;
}
return false;
     
}
