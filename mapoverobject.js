//ex1 The incrementValues function from yesterday can be written much more elegantly using map. Rewrite that function using our new and improved version of map. For convenience, the prompt is reproduced below: Write a function called incrementValues that accepts an object as a parameter, and outputs an object with all of its numeric values incremented by one. You'll 
//want to use the updated version of map for this, and you will need to check the type of each value to determine whether or not it should be incremented.
function incrementValues(object){
var result={};
 return map(object,function (number,key){
    if(typeof number=="number"){
        result[key]=number+1; 
return result[key]     
}
});
}
function each(coll, func) { 
       if (Array.isArray(coll)) { 
             for (var i = 0; i < coll.length; i++) { 
                   func(coll[i], i); 
             } 
       } else { 
             for (var key in coll) { 
                   func(coll[key], key); 
             } 
       } 
 }
 function map (coll, f ) {
var acc = [] ; 
if (!Array.isArray(coll)) {
acc = {} ; }
each(coll, function(element, key) {
acc[key] =f(element, key) ; 
}); return acc ; }
 
//ex2 Write a function called uppercaseValues that, given an
// object as a parameter, returns a new object with all of its string values converted to uppercase.
 // You'll want to make use of .toUpperCase() for this:
function uppercaseValues (object) {
return map(object, function(element, key) 
{if( typeof element=== 'string'){element= element.toUpperCase () ; return element }})

} 
//3 Write a function called countNestedKeys that, given an object where all values are also objects, 
// returns an object that contains the count of keys in each nested object, e.g.

 function countNestedKeys(object) { 
       // TODO: Your code here 
 } 
 countNestedKeys({a: {b: 1, c: 7}, f: {h: 22, g: 12, i: 24}}); 
 // => {a: 2, f: 3}
function countNestedKeys (object) {
return map(object , function(element, key) {  return Object.keys (element).length ; })

}
//4 Write a function called agesToMinutes that, 
//accepts an array of object as parameter and return an array of objects after converting the ages from years to minutes
function agesToMinutes (object) {
return map(object , function(element, key) { element.age=element.age*365*24*60 ;  return element.age }) 

}

 var people = [{ 
       name: { 
             first: 'Majd', 
             middle: 'Eddin', 
       }, 
       age: 30 
       }, { 
       name: { 
             first: 'Fatima', 
             last: 'Himmamy' 
       }, 
       age: 26 
       }, { 
       name: { 
             first: 'Sahar', 
             middle: 'MHD' 
       }, 
       age: 27 
       }, { 
       name: { 
             first: 'Nour', 
             middle: 'Eddin', 
       }, 
       age: 15 
       }, { 
       name: { 
             first: 'Ahmad', 
             last: 'Awad' 
       }, 
       age: 33 
 }];
 //more practice 
 /*1. You're working in a company for securing solutions, one of your clients noticed that their system has been hacked many times. The company saves its users information in usersData array, When you analyzed the system, You noticed that the password criteria is not set at all. You were asked to check users' account and flag and give useful information for each one. If the user's password is less that 8 characters you should add a flag property for the user object with a false value, else it will be true. You'll add another property called passwordHealth it will be either (weak, normal or strong) according to this criteria: Weak password: only small letters ,Normal password: small and numbers ,strong password: small, capital, numbers and symbols.

 Hint: Using Regular Expressions will make your life easier :) 
 var usersData = [ 
       { user: {email: 'majd@rbk.org', password: '_majd(2017)'}}, 
       { user: {email: 'fatema@rbk.org', password: '12345'}}, 
       { user: {email: 'maher@rbk.org', password: 'M@her2017'}}, 
       { user: {email: 'sahar@rbk.org', password: 'saher2017'}} 
 ]; 
 function securityCheck(object){ 
       \ your code is here 
 } 
 securityCheck(usersData); // == > 
 [ 
       { user: {email: 'majd@rbk.org', password: '_majd(2017)', flag: true, passwordHealth: 'normal'}}, 
       { user: {email: 'fatema@rbk.org', password: '12345', flag: false, passwordHealth: 'weak'}}, 
       { user: {email: 'maher@rbk.org', password: 'M@her2017', flag: true, passwordHealth: 'strong'}}, 
       { user: {email: 'sahar@rbk.org', password: 'saher2017', flag: true, passwordHealth: 'normal'}} 
 ]; 
Hint: Using Regular Expressions will make your life easier :)*/

 // draft 
function checkpasswordflag (object){ 
     var result = {} 
result.email=object.email ; 
result.password = object.password ; 
if (object.password.length < 8 ) {
result.flag = false }
else {result.flag = true } 
var  regex = /[A-Z]/g; 
var regexnumber= /[0-9]/g;
var regexsymbols = /[.*+?^$@{}()|[\]\\]/g;
var regexlowercase =  /[a-z]/g; 
console.log(result.password.match(regex))
if (result.password.includes(regexlowercase)===true)
{result.passwordHealth='weak'} 

if (result.password.includes(regexnumber)===true && result.password.includes(regex)===true ) {result.passwordHealth='normal' }

if (result.password.includes(regexnumber)===true&& result.password.includes(regex)===true&&result.password.includes(regexsymbols)===true&&result.password.includes(regexlowercase)===true) {result.passwordHealth='strong'} return result ; 

 } 

