//1 Write a function called sum that takes a number as a parameter and returns the sum of all integers up to the given number starting from 0.
function sum(number) {
   if (number===0){
   return 0;}
return number+sum(number-1);
} 
//2 Write a function called factorial that takes a number as a parameter and returns the factorial of the given number. 
//The factorial is the product of all the integers before a given number, starting with 1. The factorial of 0 is 1.
function factorial(number) {
   if (number===0){
    return 1;} 
   return number*factorial(number-1);
} 
//3 Write a function called repeatString that takes two parameters, 
//a string and a number and returns a new string with the given string repeated the given number of times.
function repeatString(string, number) {
   if (number===0){ 
    return "";}
   return string+repeatString(string,number-1);
} 
//4 Write a function called fibonacci that takes a number as a parameter and returns the nth term in the fibonacci sequence. The fibonacci sequence is: 0 1 1 2 3 5 8 13 21 .. Every number in the sequence is the sum of the previous two numbers: 5 = 2 + 3, 8 = 3 + 5, etc. The first two numbers are the exception and are equal to 1. 
//You can use this link for more information about Fibonacci https://www.omnicalculator.com/math/fibonacci
function fibonacci(number) {
   if (number===1){
    return 1}
   if (number===0){
    return 0}
    
    return fibonacci(number-2)+fibonacci(number-1)
   
} 
//5 Write a function called multiplyBy10 that takes two numbers as parameters 
//and returns the first number multiplied by 10 the amount of times specified by the second number.
function multiplyBy10(firstNumber, secondNumber) {
if (secondNumber===0){
    return firstNumber}   
if (secondNumber===1) {
    return 10*firstNumber}
   return 10* multiplyBy10(firstNumber, secondNumber-1)
} 
//more practice
//1 Write a function called sumBetween that takes two numbers (start and end) as parameters and returns the sum of the numbers from start to end. 
//What happens if the start is larger than the end? Modify the function to check for this case and, when found, swap the start and end.
function sumBetween(start, end) {
   if (start>end){
    return sumBetween(end,start);}
   if (start===end){
    return start;}
   return start+sumBetween(start+1,end) ;
} 
//2 Write a function called add that takes two numbers as parameters and returns their sum. You can only use inc and dec to accomplish this.
function add(number1, number2) {
   if (number2===0){
      return number1;}
      
   return add(inc(number1),dec(number2));
} 

//3 Write a function called isEven that takes 
//a number as parameter and returns true if that number is even, and false otherwise; You can not use the modulus % operator
function isEven(number) {
   if (number===0){
    return true;}
    if (number===1){
    return false;}
   return isEven(number-2);
}  
//4 Write a function called multiply that takes two numbers 
//as parameters and returns their multiplication You can not use the multiplication * operator, you must use repeated addition.
function multiply(number1, number2) {
   if (number2===0){
      return 0;}
      
   return number1+multiply(number1,dec(number2));
} 
//5 Write a function called range that 
//takes two numbers as parameters and returns an array of all the integers between the two numbers, excluding both numbers.
function range(start, end) {
   if (inc(start)===dec(end)){
     return inc(start);}
   return ""+inc(start)+","+range(inc(start),dec(end))+","+dec(end); 
}
//advanced
//1 Write a function called stringLength that takes a string as a parameter and returns the length 
//of that string. You cannot use the length property of the string. Instead, you'll need to use the string method called .slice().
function stringLength(string) {
   if (string===""){
    return 0;}
   return 1+stringLength(string.slice(1));
} 
//2 Write a function called modulo that takes two numbers as parameters 
//and returns the remainder after dividing the first number by the second number. You cannot use the % operator to solve this question.
function modulo(number1, number2) {
    if (number1<number2){
        return number1;}
    return modulo(number1-number2,number2);
 
} 
//3 Write a function called countChars that takes two parameters a string and 
//a character and returns a number representing the number of times that the character appears in the string. Use the .slice() method.
function countChars(string, char) {
   if (string===""){
    return 0;}
   if (string[1]===char){
     return 1+counts(string.slice(1),char);}
   return countChars(string.slice(1),char);
} 
//4 Write a function called 
//indexOf that takes two parameters, a string and a character and returns the index of the first occurance of the character in the string.
function indexOf(string, char) {
   if (string ===""){
     return 0;}
   if (string[0]===char){
    return 1+indexOf(string.slice(1),char);;
   }
   return indexOf(string.slice(1),char);
}
//5 .Write a function called power that takes two numbers (base & exponent) as parameters and returns the result of raising the base to the exponent. The power function in the lecture works but can be made considerably faster through a method known as successive squaring. 
//To get an idea of how this works, observe that: 24 = (22)2 27 = 2(23)2 28 = (24)2 Modify the power function to take advantage of this technique.
function power(base, exponent) {
  if (exponent===0){
    return 1;
  }
  
   if(exponent%2!==0){
    return base*power(base**2,(exponent-1)/2)
   }
   return power(base**2,exponent/2)
}  
//6 Write a function called reverseString that takes a string as a parameter and returns the reverse of the string.
function reverseString(string) {
   if (string===""  ) {
    return "";
   }
    return string[string.length-1]+reverseString(string.slice(0,string.length-1));
}
//7 Write a function called greatestCommonDivisor that takes two numbers as parameters and returns the greatest common divisor.
function greatestCommonDivisor(number1, number2) {
     if (number2===0){
        return number1;}
    return greatestCommonDivisor(number2,number1%number2);
} 
//8 Write a function called lowestCommonMultiple that
// takes two numbers as parameters and returns lowest common multiple Note: 
//Assume that the two numbers are greater than or equal to 2.
//draft
function lowestCommonMultiple(number1, number2) { 
  return (number1*number2)/greatestCommonDivisor(number1, number2);
} 




//9Write a function called numberOfHandshakes that takes a number of people
// in a party as a parameter and returns the total number of unique handshakes. Every person can only handshake every other person once.
//draft
function numberOfHandshakes(n) {
    if (n==2){
    return 1;
    }
   return (n-1) *numberOfHandshakes(n-1);
} 