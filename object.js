//2 Create an object that represents you. It should contain your first name, last name, age and hometown. Assign it to a variable called person.
var person={
    firstname:"hana",
    lastname:"jarraya",
    age:32
    hometown:"sfax"
}
//3 Access the favorite food property in the object using dot notation, and reassign it to a different food.
var person={
    firstname:"hana",
    lastname:"jarraya",
    age:32,
    hometown:"sfax",
    favoritefood:"rice"
}
person.favoritefood="pasta"
//4 Change your object to have a single name key, the value 
//of which is an object – this object should have first, last and middle keys containing your first, last, and middle names respectively.
var person={
    singlename: {firstname:"hana",secondname:"jarraya"},
    age:32,
    hometown:"sfax",
    favoritefood:"rice"
}
//5 Write a function called emptyObject that takes no parameters and returns an empty object.

function emptyObject(){   
    var object={
        itemName:"",
        categoryType:"",
        yearReleased:"",
        rating:"",
        price:""}
    return object; }
//6 Write a function called addProperty that takes two parameters,
// an object, and a key as a string. It then adds the key as a property in the object and gives it the value true.
function addProperty(object, key) {
    object[key]=true;
    return object;
}
//7 Write‌ ‌a‌ ‌function‌ ‌called‌ ‌‌deleteProperty‌‌ ‌
//that‌ ‌takes‌ ‌takes two parameters an‌ ‌object‌ ‌and‌ a key as a string. It then removes the property with that key from the object.
function deleteProperty(object,key){
delete object[key];
return object;
}
//8 nested object .Write‌ ‌a‌ ‌function‌ ‌called‌ ‌‌addObjectProperty‌‌ ‌that‌ ‌takes‌ ‌three‌ ‌parameters,‌
// ‌an‌ ‌object,‌ ‌a‌ ‌string as a key‌, ‌and‌ ‌a second ‌object. It then adds a new property with the passed key and the second object as its value.
function addObjectProperty(obj1,key,obj2){
        obj1[key]=obj2;
        return obj1;}
var person1 = {username : "hana" , age : 32}

var hanaImages = { img1 : "link1" , img2 : "link2"}

addObjectProperty(person1,"images",hanaImages )
//9 Write‌ ‌a‌ ‌function‌ ‌called‌ ‌‌addFullNameProperty‌‌ ‌that‌ ‌takes‌ ‌an‌ ‌object‌ ‌as‌ ‌a‌ ‌parameter.‌ ‌If the object has both a firstName and a lastName property,
// then it adds a new property with the key fullName and the value as a string of the firstName and lastName combined with a space between them.
var object={firstName:"oussema",lastName:"yahyeoui"}
function addFullNameProperty(object){
    object["fullName"]=object.firstName+" "+object.lastName;
    return object;
}
//10 Write‌ ‌a‌ ‌function‌ ‌called‌ ‌‌addArrayProperty‌‌ ‌that‌ ‌takes‌ ‌three‌ ‌parameters,‌ ‌
//an‌ ‌object,‌ ‌a‌ ‌string as a key‌ ‌and‌ ‌an‌ ‌array.‌ ‌It then adds a new property to the object with the specified key and its value is the array.
function addArrayProperty(object,key,array){
 object[key]=array;
return object;
}
//11 Write‌ ‌a‌ ‌function‌ ‌called‌ ‌‌printAllProperties‌‌ ‌that‌ takes ‌an‌ ‌object‌ ‌as a parameter. 
//It then prints out to the console the values of all the object’s properties.
function printAllProperties(object){
     for (var key in object){
     console.log(object[key]);}
}
//more practice 
//1 Write a function called removeNumbersLargerThan 
//that takes two parameters, a number and an object. It then removes all properties with values larger than the specified number
function removeNumbersLargerThan(number, object){
    for (var key in object){
      if (object[key]>number){
        delete object[key];}
}
      return object
}
//2 Write a function called removeAllEvenValues that takes an object as a parameter. It then removes all properties with values that are an even number.
function removeAllEvenValues(object){
    for (var key in object){
        if (object[key] %2===0){
        delete object[key];}
    }
    return object;
}


//3 Write a function called removePropertiesNotEqualTo10 that takes an object as a parameter. It then removes all properties that are not equal to 10.
function removePropertiesNotEqualTo10(object){
    for (var key in object){
        if (object[key] !==10){
            delete object[key];}
        
    }
    return object;
}

//4 Write a function called removeStringsLongerThan that takes two parameters,
// a number and an object. It then removes all strings with lengths larger than the number parameter.
function removeStringLongerThan(object,number){
    for (var key in object){
        if (object[key].length >number){
        delete object[key];}
    }
    return object;
}
//5 Write a function called removeAllNumbers that takes an object as a parameter. It then removes all properties in the object that have number values.
function removeAllNumbers(object){
    for (var key in object){
        if (typeof(object[key])=== "number"){
        delete object[key];}
    }
    return object;
}
//6 Write a function called removeArrays that takes an object as a parameter. It then removes all properties that have array values.
function removeArray(object){
    for (var key in object){
        if (Array.isArray(object[key])){
        delete object[key];}
    }
    return object;
}

//7 Write a function called getFirstElementOfProperty that takes an object and a key and returns the first element in the array at the given key. 
// If the array is empty it should return undefined. 
//If the property at the given key is not an array it should return undefined. If there is no property at the key it should return undefined.
function getFirstElementOfProperty(object,key){
    if(object==={})return undefined
    if(!key in object)return undefined
    if(object[key]===[])return undefined
    return object[key][0];
}
//8 Write a function called getNthElementOfProperty that takes three parameters, an object, a key and a number. 
// It then returns the element located at the index equal to the number parameter from the array at the given key. If the array is empty it should return undefined. 
//If the property at the given key is not an array it should return undefined. If there is no property at the key it should return undefined.
function getNthElementOfProperty(object,key,number){
    if (!Array.isArray(object[key]))
    {return undefined;}
    if (!(key in object)){
        return undefined;}
    return object[key][number];
}
//advanced
//1 Write a function called isPropertyPresent that takes two parameters, an object and a key. It then returns true if there is a property at 
//the given key and false otherwise.

function‌‌ ‌‌isPropertyPresent(‌object‌, key)‌ ‌{‌ ‌   ‌‌//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  var‌‌ ‌‌obj‌ ‌‌=‌‌ ‌{‌ ‌   ‌a:‌ 1,   b: 2 }  
//isPropertyPresent(obj, ‘c’) ‌‌-> false isPropertyPresent(obj, ‘b’) ‌‌-> true
return key in object;
}
//2.Write a function called getAllKeys that takes an object as a parameter and returns an array of keys of all the properties in the object.

‌‌‌‌function‌‌ ‌‌getAllKeys(‌object‌)‌ ‌{‌ ‌   ‌‌//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  var‌‌ ‌‌obj‌ ‌‌=‌‌ ‌{‌ ‌   ‌a:‌ 1,   b: 2 }  getAllKeys(obj) -> ‌‌[‘a’, ‘b’]
//HINT: Use object.keys()
//3.Write a function called getAllValues that takes an object as a parameter and returns an array of all the values from that object.
return object.keys()
}
‌‌‌‌function‌‌ ‌‌getAllValues(‌object‌)‌ ‌{‌ ‌   ‌‌//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  var‌‌ ‌‌obj‌ ‌‌=‌‌ ‌{‌ ‌   ‌a:‌ 1,   b: 2 }  getAllValues(obj) ‌‌-> ‌‌[1, 2]
//HINT: Use object.values()
return object.values();
}

//4.Write a function called transformFirstAndLast that takes an array as a parameter and returns an object with one property 
//where: The first element of the array is the key. The last element of the array is the value.

‌‌‌‌function‌‌ ‌‌transformFirstAndLast(‌array)‌ ‌{‌ ‌   ‌‌//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  var array = ['Queen', 'Elizabeth', 'Of Hearts', 'Beyonce']  
//transformFirstAndLast(array) -> ‌‌{ Queen: ‘Beyonce’ }
return {array[0]:array[array.length-1]}
}
//5.Write a function called extend that takes two objects and adds all the properties of the second object to the first object 
//if the property’s key does not already exist.

function‌‌ ‌‌extend(‌object1, object2)‌ ‌{‌ ‌   ‌‌//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  var‌‌ ‌‌obj1 = ‌‌{    ‌a:‌ 1,    b: 2 } var‌‌ ‌‌obj2 = ‌‌{    b:‌ 4,    c: 3 }  
//console.log‌(obj1) ‌‌-> ‌‌{a: 1, b: 2}  extend(obj1, obj2) console.log‌(obj1) ‌‌-> ‌‌{a: 1, b: 2, c: 3}
for (key in object2){
    object1[key]=object2[key]
}
return object1;
}
//6.Write a function called countAllCharacters that takes a string as a parameter and returns an object with each unique character 
//as a key and the value is the amount of times it appears in the string. If the string is empty it should return an empty object.

‌‌‌‌function‌‌ ‌‌countAllCharacters(‌string)‌ ‌{‌ ‌   ‌‌//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  ‌‌countAllCharacters(‘hello’)‌‌ -> ‌‌{ h: 1, e: 1, l: 2, o: 1 } ‌‌
//countAllCharacters(‘banana’)‌‌ -> ‌‌{ b: 1, a: 3, n: 2 }
unique = str.split("")
   
  // Create a set using array
unique = new Set(unique);
   
  // Conver the set into array using spread
  // operator and join it to make string
  unique = [...unique];
  var object={}
  var object={}
  for (var i=0;i<unique.length;i++){
    object[unique[i]]=string.split(unique[i]).length-1
  }
  return object;
}
//7.Write a function called countWords that takes a string as a parameter and returns an object with each unique word as a key and 
//the value is the amount of times it appears in the string If the string is empty it should return an empty object.

‌‌‌‌function‌‌ ‌‌countWords(‌string)‌ ‌{‌ ‌   ‌‌
unique = str.split(" ")
   
  // Create a set using array
unique = new Set(unique);
   
  // Conver the set into array using spread
  // operator and join it to make string
  unique = [...unique];
  var object={}
  var object={}
  for (var i=0;i<unique.length;i++){
    object[unique[i]]=string.split(unique[i]).length-1
  }
  return object;
}//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  countWords ('ask a bunch get a bunch’) -> { ask: 1, a: 2, bunch: 2, get: 1 }  ‌‌
//countWords (‘’) ‌-> ‌‌{}
//8.Write a function called convertObjectToList that takes an object as a parameter and returns an array where each element is an array 
//with the key as the first element and the value as the second.

‌‌‌‌function‌‌ ‌‌convertObjectToList(‌object)‌ ‌{‌ ‌
array=[]
for (key in object){
    array.push([key, object[key]])
}
return array;
}   ‌‌//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  var‌‌ ‌‌obj = ‌‌{    ‌name:‌ ‘holly’,    age: 35,    role: ’producer’ }  ‌‌
//convertObjectToList(obj) -> ‌ [[‘name’, ’holly’], [‘age’, 35], [‘role’, ’producer’]]
//9.Write a function called select that takes two parameters, an object and an array. It then returns a new object with properties 
//from the passed object whose keys were found in the array as elements.

‌‌‌‌function‌‌ ‌‌select(‌array, object)‌ ‌{‌ ‌   ‌‌//Write‌ ‌your‌ ‌code‌ ‌here‌ ‌ }‌ ‌  var‌‌ ‌‌arr = [‘a’, ’b’, ’e’] var‌‌ ‌‌obj = ‌‌{    a: 1,    b:‌ 2,    c: 3,    d: 4 }  
//select(arr, obj) ‌-> ‌‌{ a: 1, b: 2 }
var object2={}
for (key in object){
    if (array.includes(key)){
        object2[key]=object[key]
    }
}
return object2
}