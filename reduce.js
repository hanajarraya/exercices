 function reduce(array, f, start) { 
       var acc = start; 
       each(array, function(element) { 
             acc = f(acc, element); 
       }); 
       return acc; 
 }
 function each(coll, func) { 
       if (Array.isArray(coll)) { 
             for (var i = 0; i < coll.length; i++) { 
                   func(coll[i], i); 
             } 
       } else { 
             for (var key in coll) { 
                   func(coll[key], key); 
             } 
       } 
 }
 //1.We've shown that we can write the function sum -- which sums an array of numbers -- using reduce like this (see slides for more):

 /*function sum(numbers) { 
       return reduce(numbers, function(total, number) { 
             return total + number; 
       }, 0); 
 } 
 function product(numbers) { 
       return reduce(???, function(???, number) { 
             return ??? * number; 
       }, ???); 
 }*/
  function product(numbers) { 
       return reduce(numbers, function(total, number) { 
             return total * number; 
       }, 1); 
 }
 //2 Rewrite the averageAge function from the warmup using reduce. The function signature and people array are copied below for your convenience:

 var people = [ 
       {name: {first: 'Grace', middle: 'B.', last: 'Hopper'}, age: 85}, 
       {name: {first: 'Adele', last: 'Goldstine'}, age: 43}, 
       {name: {first: 'Ada', last: 'Lovelace'}, age: 36}, 
       {name: {first: 'Hedy', middle: 'E.', last: 'Lamarr'}, age: 85}, 
       {name: {first: 'Ruchi', last: 'Sanghvi'}, age: 34} 
 ]; 
 function averageAge(array) { 
var average;
       average= reduce(array, function(total, element) { 
             return total + element["age"]; 
       }, 0); 
return average/array.length;
 } 
 //3 The range function from yesterday looks like this:

 function range(start, end) { 
       var acc = []; 
       for (var i = start; i < end; i++) { 
             acc.push(i); 
       } 
       return acc; 
 } 
  function factorial(n) { 
var array=range(1,n+1);
return  reduce(array, function(total, element) { 
             return total * element; 
       }, 1); 

 } 
 //more practice
 //1 The sumBy function from previous lessons can be implemented using each like this:

 function sumBy(numbers, f) { 
       var sum = 0; 
       each(numbers, function(number) { 
             sum = sum + f(number); 
       }); 
       return sum; 
 } 
 //Rewrite sumBy using reduce.

 function sumBy(numbers,f) { 

return  reduce(numbers, function(total, element) { 
             return total + f(element); 
       }, 0); 

 } 

sumBy([1,2,3],function(element){return element**element}) //===>32
//2 The max function below takes an array of numbers as a parameter, and returns the maximum number in the array:

 function max(numbers) { 
       var max = numbers[0]; 
       each(numbers, function(number) { 
             if (number > max) { 
                   max = number; 
             } 
       }); 
       return max; 
 } 
 //Rewrite max using reduce.
 
function max(numbers) { 

return  reduce(numbers, function(maximum, element) {
                
             return Math.max(maximum,element); 
       }, numbers[0]); 

 } 
 //3 Write a function called countOccurrences that, given a string and a character (a string with length one) as parameters, 
 //returns the total number of times that character occurs in the string. For example:
/*
 countOccurrences('hello', 'l'); // => 2 
 countOccurrences('the', 'z'); // => 0 
 countOccurrences('hello, world!', 'l'); // => 3 */
function countOccurences(string,character) { 
var array=string.split('');
return  reduce(array, function(total, element) {
                if(element===character)
                {total= total+1; }
                return total;
       },0); 

 } 

//4 In the lecture (see slides) we ran through a function called everyNumberEven -- write functions like it that do the following:
/*
 Given an array of numbers, determine if every one of them is odd 
 Given an array of numbers, determine if every one of them is positive 
 Given an array of strings, determine if all of them have a length greather than 3 
 Given an array of strings, determines if all of them contain the letter 'e'.*/
function everyNumberOdd(numbers) { 

return  reduce(numbers, function(result, number) { 
             return result && (number%2!=0); 
       }, true); 

 } 
 function everyNumberPositive(numbers) { 

return  reduce(numbers, function(result, number) { 
             return result && (number>=0); 
       }, true); 

 } 
 function everyStringGreaterThanThree(strings) { 

return  reduce(strings, function(result, string) { 
             return result && (string.length>3); 
       }, true); 

 } 
 function everyStringContainE(strings) { 

return  reduce(strings, function(result, string) { 
             return result && (string.includes('e')); 
       }, true); 

 } 
 //5 Write a function every that takes two parameters: an array and a predicate (a function that returns true or false). every should return true if the predicate returns true for every element in the array. You should be able to use it to write everyNumberEven like this:

 function everyNumberEven(numbers) { 
       return every(numbers, function(number) { 
             return number % 2 === 0; 
       }); 
 }
//Test that it works by rewriting the functions in exercise (4) above using eve
 function every(array, f) { 
       var acc = true; 
       each(array, function(element) { 
            
           
            acc= acc && f( element); 

       }); 
       return acc; 
 }
 function everyNumberEven(numbers) { 
       return every(numbers, function(number) { 
             return number % 2 === 0; 
       }); 
 }
 function everyNumberOdd(numbers) { 
       return every(numbers, function(number) { 
                
             return (number % 2) != 0; 
       }); 
 }
 function everyNumberPositive(numbers) { 
       return every(numbers, function(number) { 
                
             return number>= 0; 
       }); 
 }
  function everyStringLenthGreaterThan3(strings) { 
       return every(strings, function(string) { 
                
             return string.length>3; 
       }); 
 }
 function everyStringContainE(strings) { 

return  every(strings, function( str) { 
               
             return  str.includes('e'); 
       }); 

 } 
 //advanced 
 //1 .Let's write a function called join that works just like the built-in join, but using reduce! If you're unfamiliar with the built-in version of join, this is how it works:

 /*['the', 'quick', 'brow', 'fox'].join(' '); // => 'the quick brown fox' 
 ['one', 'two', 'three'].join(':'); // => 'one:two:three' 
 
 Part One: First, write a function called joinWith that takes three arguments: the string to join onto, the string that will be joined, and a separator. That is, it should work like this: 
 function joinWith(onto, next, separator) { 
       // YOUR CODE HERE 
 } 
 joinWith('the quick', 'brown', ' '); // => 'the quick brown' 
 joinWith('one:two', 'three', ':'); // => 'one:two:three' 
 
 Part Two: Now, using joinWith and reduce, write join: 
 function join(array, separator) { 
       // YOUR CODE HERE 
 } 
 join(['the', 'quick', 'brown', 'fox'], ' '); // => 'the quick brown fox' 
 join(['one', 'two', 'three'], ':'); // => 'one:two:three'*/ 



 function joinWith(onto,next,separator){
    return onto.concat(separator, next);}

function join(strings,seperator) { 

return  reduce(strings, function(result, string) { 
             return joinWith(result,string,seperator); 
       },strings[0]); 

 } 
//2 Uses of reduce are not restricted to turning arrays into numbers -- use reduce to rewrite map (instead of using each).
//draft
function map (coll, f ) {

if (Array.isArray(coll)) { acc=[];
acc= reduce(coll, function(element) {
return f(element) ; 
},[]);
}else {acc={}
acc= reduce(coll, function(element) {
return f(element) ; 
},[]);
}
 
 
}
//3 We previously covered the function countWords that worked like this:
/*
 function countWords(s) { 
       var acc = {}; 
       var words = s.split(' '); 
       for (var i = 0; i < words.length; i = i + 1) { 
             var word = words[i]; 
             if (acc[word] === undefined) { 
                   acc[word] = 1; 
             } else { 
             acc[word]++; 
             } 
       } 
       return acc; 
 }
 Rewrite countWords using reduce. 
 Write the function countChars that works like countWords, but counts characters instead of words (using reduce of course).*/
function countWords(s) { 
       var acc = {}; 
       var words = s.split(' '); 
       acc= reduce(words,function (obj,word){ 
             
             if (obj[word] === undefined) { 
                   obj[word] = 1; 
             } else { 
             obj[word]++; 
             }
                return obj; 
       },{}); 
return acc;
        
 }
 function countCharacters(s) { 
       var acc = {}; 
       var characters = s.split(''); 
       acc= reduce(characters,function (obj,char){ 
             
             if (obj[char] === undefined) { 
                   obj[char] = 1; 
             } else { 
             obj[char]++; 
             }
                return obj; 
       },{}); 
return acc;
        
 }