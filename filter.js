//1 Use filter to write the following functions:

 /*Write a function called evens that takes an array of numbers as a parameter, and returns an array of only the even numbers in the parameter. 
 Write a function called multiplesOfThree that takes an array of numbers as a parameter, and returns an array of only the numbers that are multiples of three. 
 Write a function called positives that takes an array of numbers as a parameter, and returns an array of only the numbers that are positive. 
 Write a function called evenLength that takes an array of strings and returns an array of only the strings with an even length.*/
function filter(array,predicate){
var acc=[];
each(array,function(element){
        if(predicate(element)){
            acc.push(element)}});
return acc;
}

function each(coll, func) { 
       if (Array.isArray(coll)) { 
             for (var i = 0; i < coll.length; i++) { 
                   func(coll[i], i); 
             } 
       } else { 
             for (var key in coll) { 
                   func(coll[key], key); 
             } 
       } 
 }

function evens(array){
 return filter(array,function(number){
        return number%2===0})
}
function multiplesOfThree(array){
 return filter(array,function(number){
        return number%3===0})
}
function positives(array){
 return filter(array,function(number){
        return number>=0})
}
function evenLength(array){
 return filter(array,function(string){
        return string.length%2===0})
}
//more practice
//1 Use filter to write the following functions:
/*
 odds: Accepts an array of numbers, and returns the odd ones. 
 negatives: Like positives, but with negatives! 
 largerThanSix: given an array of numbers, returns those larger than 6.*/
function odds(array){
 return filter(array,function(number){
        return number%2!=0})
}
function negatives(array){
 return filter(array,function(number){
        return number<0})
}
function largerThanSix(array){
 return filter(array,function(number){
        return number>6})
}
//2 .Using filter, write a function startsWithChar that accepts two parameters: an array of strings, and a character (e.g. 'a'), and returns an array of only the strings that start with that character:
/*
 function startsWithChar(strings, character) { 
       // ... 
 } 
 var words = 'the quick brown fox jumps over the lazy dog'.split(' '); 
 startsWithChar(words, 'q'); // => ['quick'] 
 startsWithChar(words, 't'); // => ['the', 'the']*/
function startsWithChar(strings,character){

 return filter(strings,function(string){
        console.log(character);
        
        return string.charAt(0)==character})
}
//3 Extend the filter function to pass the index of each element to the predicate; then, complete the following: Write a function called 
//evenIndexedEvenLengths (yeah, a bit of a mouthful)
// that accepts an array of strings as a parameter, and returns only the strings that are found at an even index that also have an even length, e.g.:
function filter(array,predicate){
var acc=[];
each(array,function(element,index){
        if(predicate(element,index)){
            acc.push(element)}});
return acc;
}
function evenIndexedEvenLengths(strings){

 return filter(strings,function(string,index){
        
        
        return string.length%2===0&&index%2===0})
}
//4 Write a version of filter that works on arrays and objects; then, use your updated version of filter to filter an object with 
//values of mixed types to an object with only numeric values. You should be able to use your modified version of filter as follows:
function filter(coll,predicate){
var acc=[];
each(coll,function(element){
        if(predicate(element)){
            acc.push(element)}});
return acc;
}
function numbersInObject(object){

 return filter(object,function(element){
     
        return typeof element==="number"})
}
//5 Write function called moveZero that accepts an array of numbers 
//as a paramerter and returns an array of numbers with all the zero values moved to the end 'using filter'
function moveZero(numbers){ 
       var nonZeroArray= filter(numbers,function(number){
        
        return number!=0})
        var zeroArray= filter(numbers,function(number){
        
        return number===0})
return nonZeroArray.concat(zeroArray);
}
//6 Write function called strongPasswords that accepts an array of object representing usersData (email, password) 
//
//and retrun an array of objects with the strong Password only using filter. Note: Strong password is comnied of : 
//- capital letters - small letters - numbers - sumbols - at least 8 characters long

 var usersData = [ 
       { user: {email: 'majd@rbk.org', password: '_Majd(2017)'}}, 
       { user: {email: 'fatema@rbk.org', password: '12345'}}, 
       { user: {email: 'maher@rbk.org', password: 'M@her2017'}}, 
       { user: {email: 'sahar@rbk.org', password: 'saher2017'}} 
 ]; 
function strongPasswords(array){ 
 regexCapital = /[A-Z]/g;
regexLowerCase = /[a-z]/g;
regexNumbers= /[0-9]/g;
regexSymbols=/[.*+?^${}()|[\]\\]/g
       return filter(array,function(object){
        
        return (object.user.password).match(regexCapital)&&
        (object.user.password).match(regexLowerCase)&&
        (object.user.password).match(regexNumbers)
        && object.user.password.length>=8
        &&(object.user.password).match(regexSymbols)})
        

}