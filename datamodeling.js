//2
//In terms of the properties of books that you thought of, represent the following books as data:

// Harry Potter and the Sorcerer's Stone (J.K. Rowling) 
 //Romeo and Juliet (William Shakespeare) 
 //Structure and Interpretation of Computer Programs (Gerald Jay Sussman, Hal Abelson) 
 //NOTE: Did you account for the possibility of two authors? If not, update your model to handle multiple authors. 
 //Three other books (see this list for ideas)

var book3={
        title:"structure and interpretation of computer programs",
        authors:["Gerald Jay Sussam","Hal Abelson"],
        MSRP:0,
        genre:"",
        numberOfPages:0,
        description:""
}
var book1={
        title:"Harry Potter and the Sorcerer's stone",
        authors:["J.K. Rowling"],
        MSRP:0,
        genre:"",
        numberOfPages:0,
        description:""
}
 var book2={
        title:"Romeo and Juliet",
        authors:["William Shakerpeare"],
        MSRP:0,
        genre:"",
        numberOfPages:0,
        description:""
}
//3 You may have been rewriting the same type of object over and over. We need to stay DRY (Do Not Repeat).
// Write a function makeBook that takes as arguments 
//different attributes of a book and returns an object representing that book that has the proper structure (we call this a factory function).
function makeBook(title,authors,MSRP,genre,numberOfPages,description){
    return {
        title:title,
        authors:authors,
        MSRP:MSRP,
        genre:genre,
        numberOfPages:numberOfPages,
        description:description
    };
}
//4 Look at one of your book objects in the console. This is the object inspector. The object inspector is nice to have, but it will be easier to have a function to display the more important information easily. 
//Write a function called displayBook that takes a book as an argument, and returns the important information in a more readable way, for example:
function displayBook(book){
return book.title +" by "+ book.authors + " "+ book.genre+", "+book.MSRP +" "+book.numberOfPages+ " "+book.description;
}
//5 Create an array called books that holds all of the books that you created above.
var books=[book1,book2,book3];
//6 Your function displayBook can be used to display a single book as a string. Now, write a function displayBooks that, given an array of books, 
//returns a single string consisting of all of the books. Use the function displayBook to format all of the books. Each book should be numbered 
//and separated with a newline (we also call this a line break) character 
//so that each book is shown on a separate line in the console. The newline character is specified with a special escaped character in a string:
function displayBooks(books){
    //var display="";
    for (var i =0;i<books.length;i++){
        console.log( i + "."+displayBook(books[i]));
    }
//return display;
}
//7 Write a function searchBooks that, given a query and an array of books, searches the array of books for 'matching' books. 
//You will decide what way you want to write your search algorithm. Here are some things to think about: What fields will be searched? 
//
//Will you search multiple fields simultaneously (it might be best to start with one field, e.g.title)? 
//Should the search be case-sensitive? How will the search work? Will it only work from the beginning of a field, or from anywhere within? some hints:

//'Harry Potter'.toLowerCase();    // => 'harry potter' 
 //'Harry Potter'.substr(0, 5);     // => 'Harry' 
 // var query = 'Harry'; 
 //'Harry Potter'.substr(0, query.length); // => 'Harry' 
 //'Harry Potter'.indexOf('Pot');  // => 6 
 //'Harry Potter'.indexOf('dog');  // => -1
function searchBooks(query,books){
    for (var i=0;i<books.length;i++){
        if (query.toLowerCase()===books[i].title.substr(books[i].title.indexOf(query),query.length).toLowerCase()){
            return displayBook(books[i]);
        }
        
    }
}
//8 Write a function removeBook that, given a book's title and an array of books, 
//returns a new array of books that does not contain the book with the provided title.
function removeBook(title,books){
    for (var i=0;i<books.length;i++){
        if (books[i].title===title){
            books.splice(i,1);
            
        }
    }
    return books;
}
//more practice
//1 As we did before, think about what kinds of aspects of 
//movies you would like to represent. A few ideas are: Title ,Director ,Duration ,Release Date ,Actors/Actresses ,Studio(s) ,Synopsis ,Rating
//2 Make five more movie objects using the same format you decided upon.
var movie1={
    title:"harry Potter",
    director:"john",
    duration:2,
    releaseDate:2010,
    actors:[["will","david"],["lia","emma"]],
    studios:"california and venswella",
    synopsis:"",
    rating:9
}
var movie2={
    title:"Ace ventura",
    director:"jim carrey",
    duration:2,
    releaseDate:2009,
    actors:[["wael","leon"]["anne","bella"]],
    studios:"chicago",
    synopsis:"",
    rating:8
}
var movie3={
    title:"spyderman",
    director:"thom",
    duration:2,
    releaseDate:2015,
    actors:[["med","sami"],["simon","ali"]],
    studios:"fajja",
    synopsis:"",
    rating:7
}
var movie4={
    title:"battman",
    director:"david",
    duration:2,
    releaseDate:2004,
    actors:[["vax","alex"],["alicia","dena"]],
    studios:"nivada",
    synopsis:"",
    rating:6
}
var movie5={
    title:"Mr and Miss smith",
    director:"smith",
    duration:2,
    releaseDate:2004,
    actors:[["bradd pith","john"],["angelina july","amelie"]],
    studios:"colorado",
    synopsis:"",
    rating:9.5
}
//3 Write a factory function for movies. HINT: What is a factory function? We explained it above!
function makeMovie(title,director,duration,releaseDate,actors,studios,synopsis,rating){
return {
    title:title,
    director:director,
    duration:duration,
    releaseDate:releaseDate,
    actors:actors,
    studios:studios,
    synopsis:synopsis,
    rating:rating

};
}
//4 Write a function displayMovie that works like displayBook, but for movies.
function displayMovie(movie){
return movie.title +" by "+ movie.director + " "+ movie.duration+", "+movie.releaseDate +" "+movie.actors+ " "+movie.studios+" "+movie.synopsis+" "+movie.rating;
}

//5.Write a function displayCast that displays the cast of a movie, including: Role , Actor/Actress name
function displayCast(movie){
	for (var i=0;i<movie.actors.length;i++){
		return movie.actors[i][0]+", role: "+movie.actors[i][1]
	}
	
}
//6.Create an array to hold the movies that you created called movies, and add your movies to it.
var movies=[];
movies.push(movie1);
movies.push(movie2);
movies.push(movie3);
movies.push(movie4);
movies.push(movie5);
//7.As before, write a displayMovies function that works just like displayBooks.
function displayMovies(movies){
    //var display="";
    for (var i =0;i<movies.length;i++){
        console.log( i + "."+displayMovie(movies[i]));
    }
//return display;
}

//8. Calculate the average length of your movies by writing a function called averageLength that will accept an array of movies 
//as a parameter and output the average length. The difficulty of this problem is dependent on how you have chosen to store the duration.
function averageLength(movies){
	
}
//How about averageRating?
//9.How about searching your movies array? Write a function that works like searchBooks, but for movies.

//advanced
//1 Tagging System: Some books have multiple categories, have won awards, are on a best-seller list, 
//or have other special characteristics. Let's incorporate a tagging system that will allow us to represent all of these.
// Write functions addTag and removeTag that each accept a book and a tag as parameters, and either add tags or remove tags respectively.

 //Considerations: 
 //If you included a genre key, replace it with a tag. 
// What if you use addTag on a book that has no tags yet? 
 //What if you attempt to use addTag with the same tag (on the same book) multiple times? Should it be possible to have the same tag twice? 
 //Add some tags to multiple books, like 'bestseller' or 'pulitzer'. 
 //Extend 
 //Extend searchBooks to work with tags.

 //2.et's revisit your removeBooks function: what would happen if you had two books with the same title, 
 //but different authors? Would your algorithm remove both books? This is a common problem that we usually solve by 
 //providing a unique identifier for each item.

 //Modify all of your books to contain an id key with a unique value. This can be an integer or a unique string (like an ISBN). 
 //Change removeBook to use the book's id for lookups instead of its title.

 //3.Can you think of a way to write a more abstract displayItem function that works for books 
 //and movies (depending on how you have structured your objects, this may or may not work well)?

 //4. Write a more general searchItems function that accepts as parameters the query, 
 //items to search, and an array of keys that should be searched. Refactor searchMovies and searchBooks to use this function.