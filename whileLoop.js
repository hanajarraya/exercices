//1 Write a function called sumOfN that takes a number as a parameter and returns the sum of that number and all the numbers before it.
function sumOfN(n) {
   var result=0;
   while(n>0){
    result=result+n;
    n=n-1;
   }
    return result;
    
    
} 
//2 Write a function called factorialOfN that takes a whole number as a parameter and returns the factorial of that number.
function factorialOfN(n) {
   var result=1;
    while (n>0){
      result*=n;
      n--;
    }
    return result;
}
//3 Write a function called repeatString that takes two parameters, as string and a number,
// and returns that string the number of times specified in the second parameter.
function repeatString(string, num) {
   var result="";
    while(num>0){
      result+=string;
      num--;
    } 
return result;
}
//4 Write a function called countMinMax that takes two numbers as parameters and returns the range
function countMinMax(min, max) {
   var result=0;
   while(result<max-min){
        result++;
        
    }
    return result;
} 
//5  Write a function called sumMinToMax that takes 
//two numbers as parameters and returns the sum between the two integers beginning at the num1 and excluding num2.
function sumMinToMax(min, max) {
   var result=min;
   var sum=0;
    while(result<max){
        sum=sum+result;
        result++;
    } 
    return sum;
}
//6 Write a function called productMinToMax 
//that takes two numbers as parameters and returns the product between the two integers beginning at the num1 and excluding num2.
function productMinToMax(min, max) {
   var result=min;
   var product=1;
    while(result<max){
        product=product*result;
        result++;
    } 
    return product;
}
//7 Write a function called multiplyBy10NTimes that 
//takes two numbers as parameters and returns the first number multiplied by 10 the amount of times indicated by the second number.
function multiplyBy10NTimes(num, n) {
   var result=num;
   var i=0;
   while(i<n){
    result*=10;
    i++;
   }
    return result;
}
//8 Write a function called countCharAtIndex that takes three parameters, a string, an index, and another string. This function should use the index 
//to find the corresponding character in the first string and loop through the second string and count how many times that character occurs.
function countCharAtIndex(string1, index, string2) {
   var result=0;
   var i=0;
   while(i<string2.length){
      if (string1[index]===string2[i])
         {result++;}
      i++;
} 
    return result;
}
//more practice
//1 Write a function called reverseString that takes a string as an input and returns that string in reverse.
function reverseString(string) {
   var result="";
   var i=string.length-1;
   while(i>=0){
        result+=string[i];
        i--;
   } 
    return result;
}
//2 Write a function called getIndexOf that takes two parameters, a string, and a character, and returns the first position of the character in that string.
function getIndexOf(string, char) {
   var result=0;
   var i=0;
   while(i<string.length){
        if (string[i]===char)
            {result=i;
             return result;}
        i++;
   } 
    return "it does not exist";
}
//advanced
//1 Write a function called sumEven that takes two numbers as parameters and returns the sum of all even numbers starting from num1 and excluding num2.
function sumEven(number1, number2) {
   var result=0;
   var i=number1;
   
   while(i<number2){
         if (i%2===0){
            result+=i;}
         i++;}
return result;
}
//2 Write a function called primeCounter that takes a number as a parameter and returns the amount of prime numbers that occur before it.
// draft
function primeCounter(number) {
   var result=1;
   var i=1;
   
   
   while(i<number){
        
        if (!isPrime(i)){
         
         i++;}
         result++;
        i++;
     }

return result;
}
function isPrime(num){
  for(var i = 2; i < num; i++)
    if(num % i === 0) return false;
  return num > 1;
}
//3 Write a program that reads a character for playing the game of Rock-Paper-Scissors.
// If the character entered by the user is not one of 'P', 'R' or 'S', the program keeps on prompting the user to enter a new character.
//draft
function rockPaperScissors(char){
  
  while(char!="P"&&char!="R"&&char!="S"){
    Console.Write("Please enter a character: ");
    char = readLine();
    rockPaperScissors(char);
  }
  return("you win!")

}
//4 Write a function called sumOfFirstAndLast that takes 
//a number as a parameter and returns the sum of the first and last digits. Please do this without turning the number into a string.
function sumOfFirstAndLast(number) {
   var unit=number%10;
   while (number>=10){ 
    number=Math.floor(number/10);
   }
 return unit+number;
}