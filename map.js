function each(coll, f) { 
       if (Array.isArray(coll)) { 
             for (var i = 0; i < coll.length; i++) { 
                   f(coll[i], i); 
             } 
       } else { 
             for (var key in coll) { 
                   f(coll[key], key); 
             } 
       } 
 } 
 function map(array, f) { 
       var acc = []; 
       each(array, function(element, i) { 
             acc.push(f(element, i)); 
       }); 
       return acc; 
 }
 //1 Rewrite the functions firstNames, lastNames and fullNames using map as shown above.
  var people = [ 
       {name: {first: 'Grace', middle: 'B.', last: 'Hopper'}, age: 85}, 
       {name: {first: 'Adele', last: 'Goldstine'}, age: 43}, 
       {name: {first: 'Ada', last: 'Lovelace'}, age: 36}, 
       {name: {first: 'Hedy', middle: 'E.', last: 'Lamarr'}, age: 85}, 
       {name: {first: 'Ruchi', last: 'Sanghvi'}, age: 34} 
 ]; 
 function ages(people) { 
       return map(people, function(person) { 
             return person.age;}); 
 }
function firstNames(people) { 
       return map(people, function(person) { 
             return person.name.first;}); 
 }
 function lastNames(people) { 
       return map(people, function(person) { 
             return person.name.last;}); 
 }
 function fullNames(people) { 
       return map(people, function(person) { 
            if(person.name.hasOwnProperty("middle")){
             return person.name.first+" "+person.name.middle+" "+person.name.last;} return person.name.first+" "+person.name.last}); 
 }
 //2 Given the function abs that computes the absolute value, finish the invocation of map that should compute the absolute value of each number in its array argument.

 
 function abs(x) { 
       if (x >= 0) { 
             return x; 
       } 
       return -x; 
 } 
       map([1, -2, 37, -100, -8, 5], abs); 
//3 We're going to write a function maximums that, given an array of arrays of numbers as an argument, 
// returns a new array containing the maximum number of each inner array. That's a bit of a mouthful, so let's break it down:

 // our argument would look like this: 
 var sampleInput = [ // it's an array 
 [1, 3, 2], // of arrays of numbers 
 [4, 23, 100], 
 [7, 6, 3, -2] 
 ]; 
var sampleInput = [ // it's an array 
 [1, 3, 2], // of arrays of numbers 
 [4, 23, 100], 
 [7, 6, 3, -2] 
 ]; 
 /*Part One: Let's start by writing a function max that, when given an array of numbers computes the maximum number in that array. 
 You will want to use each for this. 
 function max(numbers) { 
       // YOUR CODE HERE 
 } 
 max([1, 3, 2]); // => 3 
 max([4, 23, 100]); // => 100 
 
 Part Two: Now that you have a function that computes the the maximum number in an array of numbers, 
 use map to transform each array in the sampleInput into its maximum by completing the maximums function: */
function max(numbers) { 
       var max=numbers[0];
each(numbers,function(element,i){
if(element>max){max=element}})
return max;
 } 
function maximums(arrays) { 
       return map(arrays,max);
 }
 //more practice 
 //1 .Complete the function exponentials that accepts an array of numbers as a parameter, 
 // and raises each number n to the nth power, e.g:
 function exp(number){
return number**number;
} 
function exponentials(numbers) { 
       return map( numbers,exp);
 } 
 //2 First, write a function reverse that accepts a string as a parameter and returns a reversed version of that string 
 // (you'll want to use a for loop for this). Then, use reverse to write a function called reverseWords that accepts 
 // a string as an argument, and returns a string with all of its words reversed. You'll want to use split and join 
 // 
 in both of these functions.
/*
 'hello'.split(''); // => ['h', 'e', 'l', 'l', 'o'] 
 ['h', 'e', 'l', 'l', 'o'].join(''); // => 'hello' 
 'hello world'.split(' '); // => ['hello', 'world'] 
 ['hello', 'world'].join(' '); // => 'hello world' 
 reverseWords('hello world'); // => 'olleh dlrow'
Note that reverseWords should reverse each word individually, not the entire string.*/
 function reverse(string){
var result="";
var array=[];
var arrayReversed=[]
array=string.split("");
console.log(array)
for (var i=array.length-1;i>=0;i--){
    arrayReversed[array.length-i]=array[i];
}
result=arrayReversed.join("");
return result;
}

function reverseWords(string){
var array=string.split(" ");
var arrayReversed= map(array,reverse);
return arrayReversed.join(' ');
}
//3We often want to transform an array of objects by looking up the value found under a specific key in each object; 
//for instance, consider the problem of finding the ages of all the people in the people array like we did in the first exercise:

/*Write a function called pluck that takes an array of objects and a string representing a key as parameters, 
 and returns an array of all the values found under each object using the key, e.g.: 
 pluck(people, 'age'); // => [85, 43, 36, 85, 34] 
 Your implementation should use map. */
 //draft
 function pluck(array,string){

return map(array,function(object,string){
        return object[string];
})
           }
 /*Let's say that we have a string representing a CSV (comma-separated values) file that looks like this: 
 'Grace,B.,Hopper,85/nAdele,Goldstine,43/nAda,Lovelace,36/nHedy,E.,Lamarr,85/nRuchi,Sanghvi,34/n' 
 
 And we want to parse it into an array people objects like the one above. 
 Using map and split, write a function parseCSV that accepts a CSV string as a parameter 
 and outputs an array of people objects with the exact format as shown above.*/


 //advanced 
 //1.Disclaimer: This problem is very difficult! Write a function called map2 that accepts two arrays and 
 // a function as arguments, and constructs a new array by invoking its function argument on the elements 
 // of both arrays, e.g.:

 map2([1, 2, 3], [4, 5, 6], function(a, b) { 
       return a * b; 
 }); 
 function each2(coll1,coll2, f) { 
       if (Array.isArray(coll1)) { 
             for (var i = 0; i < coll1.length; i++) { 
                   f(coll1[i],coll2[i], i); 
             } 
       } else { 
             for (var key in coll1) { 
                   f(coll1[key],coll2[key], key); 
             } 
       } 
 } 
 function map2(array1,array2,f){
      var acc = []; 
       each2(array1, array2,function(element1,element2, i) { 
             acc.push(f(element1,element2, i)); 
       }); 
       return acc;
 }
 // => [4, 10, 18]


 //2.Now, write a function called mapN that accepts an arbitrary number of arrays and a n-ary function as 
 // arguments, and constructs a new array by combining the elements of all the arrays, e.g.:

 mapN([1, 2, 3], [4, 5, 6], [2, 2, 2], function(a, b, c) { 
       return (a * b) + c; 
 }); 
 // => [6, 12, 20] 
 mapN([1, 2, 3], function(x) { 
       return x * x; 
 }) // => [1, 4, 9]
//You'll need to read about the arguments keyword and apply to complete this function.