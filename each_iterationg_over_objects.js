//0.Improved each to iterate over objects is:

 function each(coll, func) { 
       if (Array.isArray(coll)) { 
             for (var i = 0; i < coll.length; i++) { 
                   func(coll[i], i); 
             } 
       } else { 
             for (var key in coll) { 
                   func(coll[key], key); 
             } 
       } 
 }

 //1.Using our updated version of each, write the following functions: values: accepts
// an object as a parameter, and outputs an array of the object's values. keys: accepts an object as a parameter, and outputs an array of the object's keys.

 function values(object){
    var array=[];
    for (key in object){
        array.push(object[key]);
    }
    return array;
}
function keys(object){
    var array=[];
    for (key in object){
        array.push(key);
    }
    return array;
}

//2.Write a function called keysLongerThan that accepts two parameters -- 
//an object and a number -- and returns a new object with only the key/value pairs in the input object whose keys are longer than the numeric argument, e.g.:

function keysLongerThan(object,number){
var result={}
for (key in object){
    if (key.length>number){
        result[key]=object[key];
    }
}
return result;
}
// 3.Write a function called incrementValues that accepts an object as a parameter, 
// and outputs an object with all of its numeric values incremented by one. 
// You'll want to use the updated version of each for this, 
// and you will need to check the type of each value to determine whether or not it should be incremented.
 // HINT: Try entering the following expressions in to your console:

 typeof 'hello'; // => 'string' 
 typeof 1;       // => 'number' 
 typeof true;    // => 'boolean'
function incrementValues(object){
var result={};
each(object,function (number,key){
    if(typeof number==="number"){
        result[key]=number+1;      
}
});
return result;
}
//4 4.Write a function called 
//objectToArrayOrArrayToObject that takes an array or an object and transforms any array or object within the 
// array or object into an object or array.
//draft not correct yet

function objectToArrayOrArrayToObject(input){
var output={};

    each(input,function(element,key){
    	if(Array.isArray(element[key])){
       		var object={};
        	each(element[key],function(subElement,subKey){object[subKey]=subElement});
       		output[key]=object
    	}else { console.log(element[key]);
    			var array=[];
    			each(element[key],function(subElement,subKey){array[subKey]=subElement});
				output[key]=array;
			}
		})
   return output; 
}
//5 Write a function called oddEvenArray that accepts an object as parameter and return array of numbers of the key values of
// the object ( you need to arrange the new array, odd numbers will be at the beginning of the array, then the even numbers will be at the end of the array)
function oddEvenArray(object){ 
       var array=[];
       each(object,function(element,key){
            if (element%2!=0){
                array.push(element);
}
});
  each(object,function(element,key){
            if (element%2===0){
                array.push(element);
}
});
return array;
 }
 // Extra challenge: 
 //return the odd numbers ordered Ascending and the even numbers descending 
 //oddEvenArray({a:3,b:5 ,c: 4 ,d: 1, e:10, f:12 ,g:56 ,h:44 ,i:32}); // ==> [1,3,5,56,44,32,12,10,4]
 function oddEvenArray(object){ 
       var array=[];
        var arrayEvens=[];
        var arrayOdds=[];
       each(object,function(element,key){
            if (element%2!=0){
                arrayOdds.push(element);
}
});
  each(object,function(element,key){
            if (element%2===0){
                arrayEvens.push(element);
}
});
arrayOdds=arrayOdds.sort();
console.log(arrayOdds);
arrayEvens=arrayEvens.sort();
console.log(arrayEvens);
array=array.concat(arrayOdds, arrayEvens)
return array;
 }
 //6 
 //Write a function called isPrimeArray that accepts an object as parameter and return array of prime numbers in the object
function isPrime(number){
  var boolean=true;
  var j=2;
  while (j<number){
             if (number%j===0 ){
                return false
               }
              j++
              
        }
        return boolean;
}
function isPrimeArray(object){
    var array=[];
    each(object,function(element,key){
        if(isPrime(element)){
            array.push(element);
}};
return array;
}