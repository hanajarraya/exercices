//1
function myFirstFunction(){

}
myFirstFunction()
//2
function helloWorld() { 
	console.log("Hello World")
}
helloWorld()
//3
function returnFive() {
	console.log(5)
}
returnFive()
//4
function helloWorldAgain(){ 
	return "Hello World Again"
}
helloWorldAgain()
//5
function returnParameter(param1){
	return param1;
}
returnParameter(450)
//6
function add(param1,param2){
	return param1+param2;
}
add(50,50)
//7
function subtract(param1,param2){
	return param1-param2;
}
subtract(10,6);
//8
function multiply(param1,param2){
	return param1*param2;
}
 multiply(25,4);
 //9
 function stringLength(param){
 	return param.length;
 }
 stringLength("hello");
 //10
 function concatTwoStrings(param1,param2){
 	return param1+param2;
 }
concatTwoStrings("hello","world")
//11
function fullname(param1,param2){
	return param1+" "+param2;
}
fullname("hana","jarraya");
//more practice
//1
function square(param){
	return param*param;
}
square(12);
//2
function cube (param){
	return param*param*param;
}
cube(2);
//3
function charAtIndex(param,index){
	return param.charAt(index);
}
charAtIndex("hello",3);
//4
function addFourNums(param1,param2,param3,param4){
	return param1+param2+param3+param4;
}
addFourNums(4,5,98,1);
//5
function perimeterRect(width,length){
	return width*2+length*2;
}
perimeterRect(2,5);
//6
function areaRect(width,length){
	return width*length;
}
areaRect(2,3);
//7
function perimeterTriangle(param1,param2,param3){
	return param1+param2+param3;
}
perimeterTriangle(1,2,4)
//8
function areaTriangle(base,height){
	return base*height/2;
}
areaTriangle(4,6);
//9
function tripleArea(width,length){
	return width*length*3;
}
tripleArea(5,6);
//Advanced
//1
function perimeterOfCircle(radius){
	return radius*2*Math.PI;
}
perimeterOfCircle(5)
//2
function areaOfCircle(radius){
	return Math.PI*radius*radius;
}
areaOfCircle(2);
//3
function inchesToCentimeters(inches){
	return inches*2.54;
}
inchesToCentimeters(2.1);
//4
function centimetersToInches(centimeters){
	return centimeters/2.54;
}
centimetersToInches(54.2);
//5
function totalSecondsConverter(H,M,S){
	return H*60*60+M*60+S;
}
totalSecondsConverter(1,1,1);
//6
function billTotal(param){
	return param+param*9.875/100+param*0.2;
}
billTotal(10);
//7
function convertToKilometers(miles){
	return miles*1.60934;
}
convertToKilometers(100);
