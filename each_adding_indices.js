 
// 0.Improved Each is :

 function each(array, func) { 
       for (var i = 0; i < array.length; i++) { 
             func(array[i], i); 
       } 
 }
//1.Using our new version of each, write a function called indexedExponentials that, 
//when given an array of numbers as an argument, returns a new array of numbers where each number has been raised to the power of its index, e.g.:
function indexedExponentials(numbers) { 
      var array=[];
      each(numbers,function(number,i){
        array.push(Math.pow(number,i))})
      return array;
 }

 //2 Write a function called evenIndexedOddNumbers that, 
 //when given an array of numbers as an argument, returns an array of only the odd numbers with even indices.
 function evenIndexedOddNumbers(numbers) { 
      var array=[];
      each(numbers,function(number,i){
        if(number%2!=0&&i%2===0){
        array.push(number)}})
      return array;
 }
 //3 Write a function called evenIndexedEvenLengths (yeah, it is long) 
 //that accepts an array of strings as a parameter, and returns only the strings that are found at an even index that also have an even length, e.g.:
 function evenIndexedEvenLengths(strings) { 
      var array=[];
      each(strings,function(string,i){
        if(string.length%2===0&&i%2===0){
        array.push(string)}})
      return array;
 }