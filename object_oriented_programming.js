//1The old bank account example from lecture looked as follows:
/*
 function makeAccount(initial) {
     var balance = initial;
     return {
          withdraw: function(amount) {
               if (balance - amount >= 0) {
                    balance = balance - amount;
                    return 'Here is your money: $' + amount;
               }
          return 'Insufficient funds.';
          },               deposit: function(amount) {
               balance = balance + amount;
               return 'Your balance is: $' + balance;
          }
     };
}
 Refactor the above as an Account class that shares its methods across different instances.*/
function Account(initial) {
     
     var account={};
     account.balance=initial;
     account.withdraw=withdraw;
    account.deposit=deposit;
     return account;
}
function withdraw(amount) {
               if (this.balance - amount >= 0) {
                    this.balance = this.balance - amount;
                    return 'Here is your money: $' + amount;
               }
          return 'Insufficient funds.';
          }
function deposit(amount) {
               this.balance = this.balance + amount;
               return 'Your balance is: $' + this.balance;
          }
//2 Our stopwatch exercise looked like this when complete:
/*
 function makeStopwatch() {
     var stop;
     var time = 0;
     return {
          start: function() {
               stop = setInterval(function() {
               time = time + 1;
               console.log('Elapsed time: ' + time + 's.');
          }, 1000);
     },stop: function() {
               clearInterval(stop);
          },
          reset: function() {
               clearInterval(stop);
               time = 0;
          }
     };
}
As before, refactor the makeStopwatch into a Stopwatch class with shared methods.*/
function Stopwatch() {
     
     
     var watch={}
     watch.stop;
     watch.time=0;
    watch.start=start;
    watch.stopWatch=stopWatch;
    watch.reset=reset;
     
     return watch;
}
function start() {
               var time=this.time
               this.stop = setInterval(function() {
               time = time + 1;
               console.log('Elapsed time: ' + time + 's.');
          }, 1000);
     }
 function stopWatch() {
               clearInterval(this.stop);
          }
 function reset() {
               clearInterval(this.stop);
               this.time = 0;
          }
//more practice
/* 0. In this exercise we are going to make a Toaster class, and our Toaster is going to toast bread 
(because that's what toasters do). Here's a template for getting started on the Toaster class:

 function Toaster() {
     var instance = {};
// TODO: your code here      return instance;
 }
 Your task will be to implement methods that actually make this toaster work!
1. addToast: This method should accept a string representing 'toast' as a parameter, e.g. 'white bread', 
and set the toast property of the Toaster instance to that piece of toast.*/
function Toaster() {
    var instance = {};
    instance.toast;
    instance.addToast=addToast;
    return instance;
 }
function addToast(toastName){
this.toast=toastName;
}
//2 eject: This method should accept no parameters, and return one of two things:
/*
 If there is no toast in the toaster, it should return a string indicating that the toaster is empty.
 If there is toast in the toaster, it should return the toast and then set the toast property of the instance to undefined.*/

function Toaster() {
    var instance = {};
    instance.toast;
    instance.addToast=addToast;
    instance.eject=eject;
    return instance;
 }
function addToast(toastName){
this.toast=toastName;
}
function eject(){
if(this.toast===undefined){
return "the toaster is empty"
}else{var printedToast=this.toast;
     this.toast=undefined
     return printedToast;
      }
}
//3
/* start: The start method is responsible for actually toasting the toast -- we can achieve this using 
the built-in setTimeout function that accepts two parameters: a function and a time (number) in milliseconds 
(e.g. 1000 represents one second) -- setTimeout will then invoke that function once after the time has tpassed. 
setTimeout also returns a number that can be passed to clearTimeout to cancel the invocation (it works almost identically to setInterval, 
	but only executes its function argument once instead of repeatedly).

 The idea of start is to use setTimeout to:
     console.log('Ding!') after some period of time (representing the time that the toast is being 'toasted')...
     and update the instance is toast property to reflect that the toast has been toasted (for instance, 'white bread' should become 'white bread (toasted)'.
 If there is no toast in the toaster at the time that start is invoked, it should return a string indicating that toast needs to be added before it can be toasted.*/
function eject(){
if(this.toast===undefined){
return "the toaster is empty"
}else{var printedToast=this.toast;
     this.toast=undefined
     return printedToast;
      }
}
function Toaster() {
    var instance = {};
    instance.toast;
    instance.addToast=addToast;
    instance.eject=eject;
    instance.start=start;
    return instance;
 }
function addToast(toastName){
this.toast=toastName;
}
function start(){
    if(this.toast){
    var that=this
    window.setTimeout(function(){console.log('Ding!');that.toast+="(toasted)"},5000)
    
    }else {return "the toast needs to be added before toasted";}
}
//4 stop: the stop method should cancel an in-progress toaster by using clearTimeout on the return value of the invocation of setTimeout from start.
function eject(){
if(this.toast===undefined){
return "the toaster is empty"
}else{var printedToast=this.toast;
     this.toast=undefined
     return printedToast;
      }
}
function Toaster() {
    var instance = {};
    instance.toast;
    instance.time;
    instance.addToast=addToast;
    instance.eject=eject;
    instance.start=start;
    instance.stop=stop;
    return instance;
 }
function addToast(toastName){
this.toast=toastName;
}
function start(){
    if(this.toast){
    var that=this
    this.time=window.setTimeout(function(){console.log('Ding!');that.toast+="(toasted)"},7000)
    
    }else {return "the toast needs to be added before toasted";}
}
function stop(){
clearTimeout(this.time);
}
//5  Provide a way to 'name' a stopwatch when invoking makeStopwatch.
// That way, if we have multiple stopwatches running simultaneously, we know which one is which based on its name,
function Stopwatch(name) {
     
     
     var watch={}
    watch.name=name;
     watch.stop;
     watch.time=0;
    watch.start=start;
    watch.stopWatch=stopWatch;
    watch.reset=reset;
     
     return watch;
}
function start() {
               var that=this
               this.stop = setInterval(function() {
               that.time = that.time + 1;
               console.log('Elapsed time: ' + that.time + 's.'+'('+that.name+')');
          }, 1000);
     }
 function stopWatch() {
               clearInterval(this.stop);
          }
 function reset() {
               clearInterval(this.stop);
               this.time = 0;
          }
//6 Implement a 'lapping' mechanism by providing another method in the returned object called lap. 
//We will want a way to view the laps as well with a laps method, and the laps should be cleared when the stopwatch is reset. 
function Stopwatch(name) {
    var watch={}
    watch.name=name;
     watch.stop;
     watch.arrayOfLaps=[];
     watch.time=0;
    watch.start=start;
    watch.stopWatch=stopWatch;
    watch.reset=reset;
    watch.lap=lap;
    watch.laps=laps;
     
     return watch;
}
function lap(){
var laptop={}
laptop["number"]=this.arrayOfLaps.length+1;
laptop["time"]=this.time;
console.log(laptop);
this.arrayOfLaps.push(laptop);
console.log(this.arrayOfLaps);
return "Adding lap @"+laptop.time+"s ("+this.name+")"
}
function laps(){
var print=""
if(this.arrayOfLaps.length>0){
for (var i=0;i<this.arrayOfLaps.length;i++){
print+=this.arrayOfLaps[i]["number"]+". "+this.arrayOfLaps[i]["time"]+"\n"
}
}else{print+="No Laps";}
return print;
}
function start() {
               var that=this
               this.stop = setInterval(function() {
               that.time = that.time + 1;
               console.log('Elapsed time: ' + that.time + 's.'+'('+that.name+')');
          }, 1000);
     }
 function stopWatch() {
               clearInterval(this.stop);
          }
 function reset() {
               clearInterval(this.stop);
               this.time = 0;
                this.arrayOfLaps.splice(0,this.arrayOfLaps.length);
          }
//7 What happens if your stopwatch runs longer than a minute? It'll probably keep listing off the time in seconds, e.g.
/*
 Elapsed time: 72s. It would be nice if it displayed minutes and seconds when the time ran beyond a single minute, e.g.
 Elapsed time: 1m 12s. Modify your stopwatch code to achieve this.*/
function Stopwatch(name) {
    var watch={}
    watch.name=name;
     watch.stop;
     watch.arrayOfLaps=[];
     watch.time=0;
    watch.start=start;
    watch.stopWatch=stopWatch;
    watch.reset=reset;
    watch.lap=lap;
    watch.laps=laps;
    watch.convert=convert;
     
     return watch;
}
function lap(){
var laptop={"number":0,"time":0}
laptop["number"]=this.arrayOfLaps.length;
laptop["time"]=this.time;
console.log(laptop);
this.arrayOfLaps.push(laptop);
console.log(this.arrayOfLaps);
return "Adding lap @"+laptop.time+"s ("+this.name+")"
}
function laps(){
var print=""
if(this.arrayOfLaps.length>0){
for (var i=0;i<this.arrayOfLaps.length;i++){
print+=this.arrayOfLaps[i]["number"]+". "+this.arrayOfLaps[i]["time"]+"\n"
}
}else{print+="No Laps";}
return print;
}
function convert(time){
return Math.floor(time/60) +"m"+time%60+"s."
}
function start() {
               var that=this
               this.stop = setInterval(function() {
               that.time = that.time + 1;
               console.log('Elapsed time: ' + convert(that.time)+'('+that.name+')');
          }, 1000);
     }
 function stopWatch() {
               clearInterval(this.stop);
          }
 function reset() {
               clearInterval(this.stop);
               this.time = 0;
                this.arrayOfLaps.splice(0,this.arrayOfLaps.length);
          }
//advanced 
//1 What should happen if we attempt to toast some toast that has already been toasted?
// Modify start so that, if the toast is already toasted, it becomes burnt on subsequent invocations of start.
function eject(){
if(this.toast===undefined){
return "the toaster is empty"
}else{var printedToast=this.toast;
     this.toast=undefined
     return printedToast;
      }
}
function Toaster() {
    var instance = {};
    instance.toast;
    instance.time;
    instance.addToast=addToast;
    instance.eject=eject;
    instance.start=start;
    instance.stop=stop;
    return instance;
 }
function addToast(toastName){
this.toast=toastName;
}
function start(){
    if(this.toast){
    var that=this
    this.time=window.setTimeout(function(){console.log('Ding!');
if(that.toast.includes("(toasted)")){
that.toast=that.toast.replace("(toasted)","(burned)")}else{that.toast+="(toasted)"}},7000)
    
    }else {return "the toast needs to be added before toasted";}
}
function stop(){
clearTimeout(this.time);
}