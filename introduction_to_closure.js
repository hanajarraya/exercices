//1  Let's make a counter using closures. For our purposes, a counter is simply a function that, when invoked, returns a number that is one higher than it used to be. For example, this is the behavior that we want:
/*
 counter(); // => 1 
counter(); // => 2 
counter(); // => 3 
We could implement this using the global scope like this: var count = 0;
 function counter() {
      count = count + 1;
      return count;
 }
 But now that we know about closures, we can do something way cooler. Finish the implementation of makeCounter below so that we can make multiple counters, each with their own internal count using closures.
function makeCounter() {
      // YOUR CODE HERE
}*/
function makeCounter() {
      var count = 0;
 function counter() {
      count = count + 1;
      return count;
 }
return counter;
}
//2 Update makeCounter so that, instead of always starting from zero, you can start from any number, e.g.:
/*
 var counter = makeCounter(100);
      counter(); // => 101;*/
function makeCounter(initial) {
      var count = initial;
 function counter() {
      count = count + 1;
      return count;
 }
return counter;
}
//3 One way we can use closures is as functions that construct other functions. Consider the numerous examples of exponentiation functions that we've created, e.g. square and cube. The following function pow is incomplete:
/*
 function pow(exponent) {
     return function(???) {
          return ???
          }
     } 
Fill in the ??? so that it works like this: 
var square = pow(2);
 var cube = pow(3); 
var power4 = pow(4); 
square(5); // => 25 
cube(3);   // => 27 
power4(4); // => 256*/
function pow(exponent) {
     return function(number) {
          return number**exponent;
          }
     } 
//more practice
//1 zipWith takes a function and two arrays and zips the arrays together, applying the function to every pair of values.The function value is one new array.If the arrays are of unequal length, the output will only be as long as the shorter one.(Values of the longer array are simply not used.)Inputs should not be modified.
/*
 zipWith( Math.pow,[10,10,10,10], [0,1,2,3] ) => [1,10,100,1000] 
zipWith( Math.max,[1,4,7,1,4,7], [4,7,1,4,7,1] ) =>  [4,7,7,4,7,7] 
 zipWith( function(a,b) {
      return a+b;
 }, [0,1,2,3], [0,1,2,3] ) => [0,2,4,6]  Both forms are valid.*/
function zipWith(f,array1,array2){
var length;
var array=[];
if(array1.length<array2.length){
    length=array1.length
}else{
    length=array2.length}
for(var i=0;i<length;i++){
    array.push(f(array1[i],array2[i]))
}
return array;
}

//2
/*We want to create a function that will add numbers together when called in succession

 add(1)(2)(); // returns 3
We also want to be able to continue to add numbers to our chain.
add(1)(2)(3)(); // 6
add(1)(2)(3)(4)(); // 10
add(1)(2)(3)(4)(5)(); // 15
and so on.*/
function add(x){
    let sum = x;
    return function resultFn(y){
        if(arguments.length){ //not relying on falsy value
            sum += y;
            return resultFn;
        }
        return sum;
    }
}
//advanced 
//Try solving this without extra ()

 //add(1)(2)(3);
var add = (function() {
    var factory = function(value) {
        var fn = function(num) {
            return factory(value + num);
        };
        // This is the main hack: 
        // We will return a function that when compared / concatted will call .toString and return a number.
        // Never use this in production code...
        fn.toString = function() {
            return value;
        };
        return fn;
    };
    return factory(0);
})();