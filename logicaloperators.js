//6
function opposite(boolean){
    return !boolean;
}

//7
function greaterThan5(num1, num2) {
   return num1>5 ||num2>5; 
} 
//8
function allGreaterThan5(num1, num2) {
   return num1>5 && num2>5;
}
//9
function largerThan5AndLessThan20(number) {
   return number>5 && number <20; 
} 
//10
function not6AndLessThan10(number) {
   return number !==6 && number <10; 
} 
//11
function largerThan3AndLessThan18(num1, num2, num3) {
   return num1>3 && num1<18 && num2>3 && num2<18 && num3>3 && num3<18 ;
}
//12
function cloudyAndRainy(string1, string2) {
   return string1==="cloudy" && string2==="rainy";
}
//13
function weatherActivities(weather) {
   if (weather ==="rainy"){          
     return "hot chocolate";
  }
   else if (weather ==="sunny"){
      return "running";
         } 
   else if (weather  === "snowy"){
       return "skiing";
          }
   else if (weather ==="windy"){
       return "wear a coat";
          } 
} 
//14
function evenAndGreaterThan7(number) {
   return number  % 2===0 && number >7;
}
//more practice
//1
function areValidCredentials(name, password) {
    return name.length>3 && password.length>=8;
}
//2
function findMinLengthOfThreeWords(string1, string2, string3) {
   if (string1.length<string2.length && string1.length<string3.length ){
    return string1.length;
   }else if (string2.length<string1.length && string2.length<string3.length ){
    return string2.length;
   }else if (string3.length<string1.length && string3.length<string2.length ){
    return string3.length;
   }
}
//3
function guessMyNumber(number) {
   return number=== Math.floor(Math.random()*6 );
}
//advanced
//1
function or(statement1, statement2) {
   return ! (!statement1 && ! statement2);
} 
//2
function and(statement1, statement2) { 
  return !(!statement1 || !statement2); 
}
//3
function shirtWidth(width) {
   if (width >= 20 && width <30){
        return "You should select a size S";
   }
   else if ( width >=30 && width < 40){
        return "You should select a size M";   
   }
   else if (width >=40 && width <50){
        return "You should select a size L" }
   return "You should select a different shirt";
}
//4 
function player1Choice(choice) {
   if (choice==="rock" || choice==="paper" || choice==="scissors") {
   	return "Player 1 chose " + choice
   }
   	else {return "Player 1 chose poorly";}
} 

//5

function convertScoreToGrade(score){
    if (score>=90 && score<= 100){
        return "A";
    }
    else if (score <=89 && score >=80){
        return "B";
    }
    else if (score <= 79 && score >=70){
        return "C"
    }
    else if (score <= 69 && score >=60){
        return "D";
    }
    else if (score <= 59 && score >=0){
        return "F"
    }
    else return "Invalid score";
}

//6

function convertScoreToGradeWithPlusAndMinus(score){
      if (score>=95 && score<= 100){
        return "A+";
    }
    else if (score <=94 && score >=90){
        return "A-";
    }
    else if (score <= 89 && score >=87){
        return "B+"
    }
    else if (score <= 86 && score >=80){
        return "B-";
    }
    else if (score <= 79 && score >=77){
        return "C+";
    }
    else if (score <= 69 && score >=67){
        return "C-";
    }
    else if (score <= 66 && score >=60){
        return "D+";
    }
    else if (score <= 59 && score >=0){
        return "D-"
    }
    else return "Invalid score";
}

//7

  function isItTruthy(value) {
   if (value){
    return "Input is truthy";
   } else {
    return "Input is falsy";
   }
} 

//8

function checkArea (area){
  return area >48 && area <100;
}

//9

function checkMultiply(num1, num2) {
   return num1*num2 >50 && num1*num2<150; 
} 

